In this project I will attempt to build a machine learning algorithm that will allow me to recognize if the text messages sent are either Spam or Legitamte.

The predecessor of this project involves producing the same output, however it is built linearly which will decrease the ability to recognize complexity in data. This project solves this issue by using deep learining techniques to allow faster and more efficient data handiling and manipulation with the help of tensorflow.

Before starting the script please make sure to:

Download the following modules:

1) Tensorflow (v.2)
2) Pandas

As well as the following dataset:

1) SMSSpam collection

Please make sure to change the destination data while reading the datasets from pandas for the scipt to actually work.


All data rights reserved to their original owners.

