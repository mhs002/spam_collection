import tensorflow as tf
import pandas as pd
import numpy as np
import time 

from numpy import loadtxt
from tensorflow import keras

df_train = pd.read_table("{}".format("/home/mohammad/Desktop/machine_learning_projects/Data_Science_Project_1/DataScience/SMSSpamCollection"),names=['Labels','SMS'])
df_labels = df_train.pop("Labels")

# print(df_train.head()) Comment out this line to check how the data frame is now set

for number in range(df_labels.size): df_labels[number] = 0 if df_labels[number] == 'ham' else 1 # Converts the strings into numbers

# Converts the list into np array

df_lables = np.asarray(df_labels)
df_labels = df_labels.astype(np.float)


big_arr = loadtxt('database.csv',delimiter=',') # Load the dataset 


model = keras.models.Sequential([
    keras.layers.Flatten(input_shape=(1,)),
    keras.layers.Dense(128,activation="relu"),
    keras.layers.Dense(2,activation="softmax")
    ])

model.compile(
        optimizer="adam",
        loss="sparse_categorical_crossentropy",
        metrics=['accuracy']
        )

model.fit(big_arr,df_labels,epochs=5)

print('{}'.format("Finished the test"))
