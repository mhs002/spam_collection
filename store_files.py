import pandas as pd
import numpy as np
from numpy import savetxt
import time 

path =  "/home/mohammad/Desktop/machine_learning_projects/Data_Science_Project_1/DataScience/SMSSpamCollection"
file_name = "database.csv"

df_train = pd.read_table("{}".format(path),names=['Labels','SMS'])


list_of_sentences = []

for line in df_train["SMS"]: list_of_sentences.append(line)

# This array contains all the sentences 
big_arr = np.array([])

# Measures the time taken to execute the transformation
start = time.time()

# Extract each sentence as row and add inside the array
def array_to_numpy(number=0,big_arr=big_arr):
    for line in list_of_sentences:
        sentence = np.asarray(line)
        sentence = np.expand_dims(sentence,axis=0)
        big_arr = np.append(big_arr,sentence,axis=0) if number < 1 else np.vstack((big_arr,sentence))
        if number == 0: number = number + 1 
    return big_arr

big_arr = array_to_numpy()

number = -1

# Changes the sentences into numeric sums
for sentence in big_arr:
    for word in sentence[0].split(): big_arr[number] = sum(ord(letter) for letter in word)
    number = number + 1

end = time.time()

# Casts the strings of numbers into floats
big_arr = big_arr.astype(np.float)

big_arr = big_arr / np.max(big_arr)

savetxt(file_name,big_arr,delimiter=',')

print("{0},{1}".format("Executed in: ",end-start))

